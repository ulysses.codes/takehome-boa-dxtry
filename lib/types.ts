import {PosePayload} from '../lib/stream';

export type Pose = PosePayload | "disconnected";
