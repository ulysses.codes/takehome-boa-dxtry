# Submission to Dexory docs

## Goals

My primary goal was to meet the following specs from the README:

1. Show an icon on top of the map image in the location provided by the pose stream
2. Allow the user to stop and start the virtual robot
3. Make clicking on the map update the server's pose through the websocket stream
4. Test requirements for the app.
5. Make the pose copyable

In addition, my personal goals for the project were:

1. Implement both a 2D and 3D solution.
2. Provide feedback for invalid locations if the user tries to place the robot on a shelf.
3. Create a layout that works for both desktop and mobile settings
4. Minimize the design footprint of the robot controls

## Design

The application is a single page which displays the pose status of the robot, a map, and controls for the robot. 

### Map 

The map display can be toggled between 2D and 3D views. In the 2D view, a triangle indicates the robots position. In the 3D view, an extruded triangle represents the bot and extruded geometry represent shelves in the warehouse.

The user can click the 2D map to move the robot to a different desired location.

### Robot status

The robot status is displayed in the top left of the map. Clicking the copy icon will copy the current pose to the clipboard - this is implemented with the clipboard API which requires a secure context and might not be available in all browsers.

### Robot controls

The robot controls are simple - a single button which will either pause or resume robot movement. If the robot is not connected, they will not appear.

### Responsiveness

The controls are moved to the bottom on a mobile browser.

## Tests

The tests are limited to the items from the README, but the infrastructure is there to build them out further:
- Checks that the 2d map uses the pose passed in as a prop. This makes sure the math is right for y flipping.
- Checks that the robotcontrol sends an unpause message to a mocked websocket server. This requires a. props are passed through correctly, and b. the button works

## Challenges

### Preprocessing

Both the 3D visualization and preventing the robot from being put on a shelf required knowing where the shelves were. `shape_detect/detect.py` uses OpenCV to create a map of the shelves by blurring map.png and thresholding it. It outputs both the thrsholded map and the shapes of the shelves as json which can then be read by the front-end app. 

After testing the first iteration of them, I was left with a huge shape around everything as well as the inner shelves. I had to use the `hierarchy` information to cull the shape which mapped to the outermost contour.

### 3D

Getting the 3D right was fun. I generated the .json files then loaded them up in https://nodysseus.io/ to iterate quickly on what worked and looked good for presentation. In the end just extruding the base lines looked good, and adding a slight transparency to them allows the user to see the bot even if it's inside a shelf.

### Testing

There are a lot of diffferent testing libraries for react and three.js. In the end what seemed to work was a mix of `jest`, `@testing-library/react`, some babel presets and preprocessors, and the settings from next.js. I added `data-testid` to the components which were tested so that they could be found by the testing framework.

## Extensions

If I were to continue this project, the features I would want to add would be (in no particular order):

- Preventing the robot from moving onto shelves server-side
- A redesign that is more sleek and includes server connection status. I would possibly move the controls onto the map itself.
- Adding tests for as much of the user functionality as possible and unit tests for the backend
- A history of pose locations - both a scrolling log (sampled every x seconds) and a path in the map displays
- Adding useful information around shelves - what is stored on them, which one the robot is close to, etc
