import cv2
import numpy as np
import json
from matplotlib import pyplot as plt
# thanks stackoverflow!
class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def detect_contours():
    # from geeksforgeeks
    img = cv2.imread("../public/images/map.png")
    grey = cv2.cvtColor(cv2.blur(cv2.blur(img, (10, 10)), (10,10)), cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(grey, 220, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


    # filter for the second hierarchical contour to get rid of the box around everything - mine
    outercontours = [v for k, v in enumerate(contours) if hierarchy[0][k][3] >= 0]

    #json encoding - mine
    with open("thresholdmap.json", "w") as file:
        file.write(json.dumps(cv2.resize(thresh, dsize=(200,200)), cls=NumpyEncoder))

    with open("contours.json", "w") as file:
        file.write(json.dumps(outercontours, cls=NumpyEncoder))

    
    # from geeksforgeeks
    i = 0
    for contour in outercontours:
        if i == 0:
            i = 1
            continue

        approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        cv2.drawContours(img, [contour], 0, (0, 0, 255), 5)

        M = cv2.moments(contour)
        if M['m00'] != 0.0:
            x = int(M['m10']/M['m00'])
            y = int(M['m01']/M['m00'])


    cv2.imshow('shapes', img)
      
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    detect_contours()
