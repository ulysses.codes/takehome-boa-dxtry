import { it } from '@jest/globals'
import React from 'react';
import {render, screen} from '@testing-library/react';
import { enableFetchMocks } from 'jest-fetch-mock';
import '@testing-library/jest-dom'

import MapDisplay from './MapDisplay';

const fetchresponses = {
  "thresholdmap.json": [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
  "contours.json": [[[0, 0], [1, 0], [1, 1], [0, 1]]]
}

enableFetchMocks()

fetch.mockResponse(req => Promise.resolve(JSON.stringify(fetchresponses[req.url])))

beforeEach(() => {
    Object.defineProperty(global, "ResizeObserver", {
        writable: true,
        value: jest.fn().mockImplementation(() => ({
            observe: jest.fn(() => "Mocking works"),
            unobserve: jest.fn(),
            disconnect: jest.fn(),
        })),
    });
});

it('uses the passed in pose', async () => {
  const x = 10;
  const y = 10;
  render(<MapDisplay pose={{x, y, angle: Math.PI}}/>)
  const imageScale = 720 / 1660;
  const adjustedx = x * imageScale * 10;
  const adjustedy = (1080 - y * 10) * imageScale;
  expect(screen.queryByTestId('svgbot')).toHaveAttribute('transform', `translate(${Math.floor(adjustedx)}, ${Math.floor(adjustedy)}) rotate(180)`)
})
