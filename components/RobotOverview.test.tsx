import { it } from '@jest/globals'
import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import '@testing-library/jest-dom'
import WS from 'jest-websocket-mock';
import { enableFetchMocks } from 'jest-fetch-mock';


import RobotOverview from './RobotOverview';

const fetchresponses = {
  "thresholdmap.json": [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
  "contours.json": [[[0, 0], [1, 0], [1, 1], [0, 1]]]
}

enableFetchMocks()

fetch.mockResponse(req => Promise.resolve(JSON.stringify(fetchresponses[req.url])))

beforeEach(() => {
    Object.defineProperty(global, "ResizeObserver", {
        writable: true,
        value: jest.fn().mockImplementation(() => ({
            observe: jest.fn(() => "Mocking works"),
            unobserve: jest.fn(),
            disconnect: jest.fn(),
        })),
    });
});

it('sends paused to the websocket', async () => {
  const server = new WS("ws://localhost/api/paused")
  const _ = new WS("ws://localhost/api/pose")
  render(<RobotOverview />)

  await server.connected

  server.send(JSON.stringify({paused: false}))
  fireEvent.click(screen.getByTestId('pausebutton'))

  await expect(server).toReceiveMessage(JSON.stringify({ paused: true }))
})

//TODO: implement test for move bot
