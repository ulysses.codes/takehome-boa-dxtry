import styles from "./RobotStatus.module.css";

import React from 'react';

import { Pose } from '../lib/types';

const formatNumber = (num: number) => num.toFixed(2);

const RobotStatus = ({ pose }: { pose: Pose }) => {

  const copy = evt => {
    evt.nativeEvent.stopPropagation()

    const data = JSON.stringify(pose);
    navigator.clipboard.writeText(data)
  }

  return pose === "disconnected" ? (<code>:(</code>) : (
    <div className={styles.container}>
      <code className={styles.pose}>x={formatNumber(pose.x)}, y={formatNumber(pose.y)}, angle={formatNumber(pose.angle)}</code>
      <ion-icon name="copy" onClick={copy}>copy</ion-icon>
    </div>
  )
};

export default RobotStatus
