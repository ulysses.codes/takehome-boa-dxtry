import styles from './mapDisplay.module.css'

import React, {useEffect, useRef, useState, useMemo, Suspense} from 'react'
import dynamic from 'next/dynamic';
import { Canvas, useThree } from '@react-three/fiber';
import { useTexture } from '@react-three/drei'
import * as THREE from "three";

import { Pose } from '../lib/types';

const RobotStatus = dynamic(() => import('../components/RobotStatus'), { ssr: false });

// statically computed constants
const imageSize = [1660, 1080];
const imageAspect = imageSize[0] / imageSize[1];
const pixelMetreScale = 0.1;
const metreSize = imageSize.map(v => v * pixelMetreScale);
const zoffset = -175


// Constructs a "shelf" from the outline provided by contours
const Shelf = ({vertices, index}) => {
  const mesh = useRef();
  const shape = useMemo(() => {
    const shape = new THREE.Shape().moveTo(...vertices[0])
    vertices.slice(1).forEach(v => shape.lineTo(...v));
    return shape;
  }, [vertices])
  return (
    <mesh key={`shape-${index}`} ref={mesh} position={[0, 0, zoffset]}>
      <extrudeGeometry attach="geometry" args={[shape, {depth: 4}]}/>
      <meshStandardMaterial color="blue" opacity={0.7} transparent={true} />
    </mesh>
  )
}

// Floor and CameraMove need to be their own elements so they can be within the Canvas context
const Floor = ({metreSize}) => {
  const [colorMap] = useTexture(["images/map.png"])
  const mesh = useRef();
  return (
    <Suspense fallback={null}>
      <mesh ref={mesh} position={[metreSize[0] * 0.5, metreSize[1] * 0.5, zoffset]}>
        <planeGeometry args={metreSize} />
        <meshStandardMaterial map={colorMap} />
      </mesh>
    </Suspense>
  )
}

const CameraMove = ({pose}) => {
  const {camera} = useThree();
  useEffect(() => {
    camera.position.x = pose.x;
    camera.position.y = pose.y - 10;
  }, [camera, pose])

  useEffect(() => {
    camera.position.z = -160;
    camera.rotation.x = Math.PI / 4;
  }, [camera])
  return (<></>)
}

// Displays the map.
const MapDisplay = ({ width, pose, moveBot, showError }: {
  pose: Pose,
  moveBot: (x, y) => void,
  showError: (err: string | false) => void
}) => {
  const [dragging, setDragging] = useState<boolean>(false);
  const [vertices, setVertices] = useState<THREE.Vector3[]>([]);
  const [displayType, setDisplayType] = useState<"2D" | "3D">("2D");
  const [thresholdmap, setThresholdmap] = useState();
  const [contours, setContours] = useState();
  const bot3D = useRef();

  width = width ?? document.documentElement.clientWidth > 700 ? document.documentElement.clientWidth * 0.7 : document.documentElement.clientWidth;
  const height = width / imageAspect;


  // assume image width
  const scale = width / imageSize[0];
  const scaledSize = imageSize.map(v => v * scale);

  // convenience object for the scale of the image
  const scaledSizeStyle = {width: `${scaledSize[0]}px`, height: `${scaledSize[1]}px`};

  //
  // grab the thresholdmap
  useEffect(() => {
    fetch("thresholdmap.json").then(res => res.json()).then(setThresholdmap);
    fetch("contours.json").then(res => res.json()).then(setContours);
  }, [])


  // adjust pose to scaled coordinates and metres -> pixels
  const adjustedPose = [pose.x, imageSize[1] * pixelMetreScale - pose.y].map(v => v * scale / pixelMetreScale);


  // scale the offset for pixels -> metres
  // and adjust the y based on imagesize
  const moveBotEvent = evt => {
    setDragging(true)
    const scaledPos = [evt.nativeEvent.offsetX / scaledSize[0], evt.nativeEvent.offsetY / scaledSize[1]];
    const isBlocked = thresholdmap[Math.floor(scaledPos[1] * thresholdmap.length)][Math.floor(scaledPos[0] * thresholdmap[0].length)] == 0;
    if(isBlocked) {
      showError("That position is blocked!")
    } else {
      showError(false)
      moveBot(evt.nativeEvent.offsetX * pixelMetreScale / scale, (imageSize[1] - evt.nativeEvent.offsetY / scale) * pixelMetreScale)
    }
  }

  // handle dragging too
  const moveBotDragEvent = evt => dragging && moveBotEvent(evt)
  const moveBotUpEvent = evt => setDragging(false)


  // only update the vertices when they change because it's a heavy call
  useEffect(() => {
    setVertices((contours ?? []).map((c, i) => c.map(cv => [cv[0][0], imageSize[1] - cv[0][1]].map(v => v * pixelMetreScale))));
  }, [contours])

  // construct the robot in 3D
  // TODO: use this for the triangle path in the svg too with a generic moveto / lineto
  const botBase = [[0, -1], [1, 1], [-1, 1]];
  const botShape = new THREE.Shape();
  botShape.moveTo(...botBase[0]);
  botBase.slice(1).forEach(v => botShape.lineTo(...v));

  return (
    <div 
      className={ styles.container } 
      style={Object.assign({ touchAction: "none" /* to work on touchscreens */ }, scaledSizeStyle)} 
      onPointerDown={moveBotEvent} 
      onPointerMove={moveBotDragEvent} 
      onPointerUp={moveBotUpEvent}
      onPointerOut={moveBotUpEvent}
      >
      <div style={Object.assign({position: "absolute", zIndex: -1, display: displayType === "3D" ? "block" : "none"}, scaledSizeStyle)}>
        <Canvas>
          <CameraMove pose={pose} />
          <ambientLight intensity="0.1" />
          <directionalLight />
          <group>
            {vertices.map((vs, i) => (<Shelf vertices={vs} index={i} key={`shelf-${i}`} />))}
          </group>
          <Floor metreSize={metreSize} />
          <mesh ref={bot3D} position={[pose.x, pose.y, -175]}>
            <extrudeGeometry args={[botShape, {depth: 2}]} />
            <meshStandardMaterial color="red" />
          </mesh>
        </Canvas>
      </div>
      <svg style={{position: "absolute", zIndex: -1, display: displayType === "2D" ? "block" : "none"}} width={scaledSize[0]} height={scaledSize[1]}>
        <defs>
          <path id="triangle" d="M 0 -10 L 10 10 L -10 10 z" fill="red" />
        </defs>

        <image href="images/map.png" width={scaledSize[0]} height={scaledSize[1]} />
        {pose !== "disconnected" && (
          <use xlinkHref="#triangle" transform={`translate(${adjustedPose.map(v => Math.floor(v)).join(", ")}) rotate(${pose.angle * 180 / Math.PI})`} data-testid="svgbot"/>
        )}
      </svg>
      <div style={{backgroundColor: "white", width: "50%"}}>
        <RobotStatus pose={pose} />
        <div className={ styles.displayswitcher }>
          <span className={displayType === "2D" ? styles.chosen : undefined} onClick={() => setDisplayType("2D")}>2D</span>
          <span className={displayType === "3D" ? styles.chosen : undefined} onClick={() => setDisplayType("3D")}>3D</span>
        </div>
      </div>
    </div>
  )
}

export default MapDisplay;
