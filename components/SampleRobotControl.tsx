import dynamic from 'next/dynamic';
import styles from './RobotControl.module.css';

import { useCallback } from 'react';

import { Pose } from '../lib/types';
import { getPausedStream, useStream } from '../lib/stream';


const RobotStatus = dynamic(() => import('../components/RobotStatus'), { ssr: false });


const RobotControl = ({pose}: {
  pose: Pose
}) => {
  // Pause state
  const { connected: pausedConnected, stream: pausedStream } = useStream(getPausedStream);

  // Connection Control
  const Control = pose === "disconnected" ? (
    <div className={ styles.disconnected }>
      Connection offline
    </div>
  ) : (
    <div className={ styles.connected }>
      Connection online
    </div>
  );

  // Pause and unpause buttons
  const pause = useCallback(() => pausedStream?.send({ paused: true }), [pausedStream]);
  const unpause = useCallback(() => pausedStream?.send({ paused: false }), [pausedStream]);
  const pauseButton = pausedConnected ? (
    <div className={ styles.buttons }>
      <button onClick={ unpause }>Start moving</button>
      <button onClick={ pause }>Stop moving</button>
    </div>
  ) : null;

  // Component content
  return (
    <div className={ styles.container }>
      { Control }
      <RobotStatus pose={pose} />
      { pauseButton }
    </div>
  );
};


export default RobotControl;
