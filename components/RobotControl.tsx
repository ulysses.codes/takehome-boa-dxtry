import dynamic from 'next/dynamic';
import styles from './RobotControl.module.css'

import React from 'react';

import { Pose } from '../lib/types';


const RobotStatus = dynamic(() => import('../components/RobotStatus'), { ssr: false });


const RobotControl = ({paused, pose, togglePause, error}: {
  paused: boolean,
  togglePause: () => void,
  pose: Pose,
  error: string | false
}) => {
  return (
    <div className={styles.container}>
      <div className={styles.movement}>
        <h3>Robot movement</h3>
        <div className={styles.controls}>
          <ion-icon name={paused ? "play" : "pause"} onClick={togglePause} data-testid="pausebutton"></ion-icon>
        </div>
      </div>
      {error && (<div className={ styles.error }>{ error }</div>)}
    </div>
  )
}

export default RobotControl;
