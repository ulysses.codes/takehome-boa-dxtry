import dynamic from 'next/dynamic';
import styles from './RobotOverview.module.css'

import React, { useCallback, useState } from 'react';

import { getPoseStream, getPausedStream, useStream, PosePayload } from '../lib/stream';
import { Pose } from '../lib/types';


const RobotControl = dynamic(() => import('../components/RobotControl'), { ssr: false });
const MapDisplay = dynamic(() => import('../components/MapDisplay'), { ssr: false });


const RobotOverview = () => {
  const [pose, setPose] = useState<PosePayload>();
  const [paused, setPaused] = useState<PosePayload>();
  const [error, setError] = useState<string | false>(false);

  // Current pose
  const { connected: poseConnected, stream: poseStream } = useStream(getPoseStream, setPose);

  // calculate a single pose value to pass through
  const passedPose: Pose = poseConnected && pose ? pose : "disconnected";

  const { connected: pausedConnected, stream: pausedStream } = useStream(getPausedStream, (payload) => setPaused(payload.paused));

  const togglePause = useCallback(() => {
    pausedStream?.send({paused: !paused});
  }, [pausedStream, paused])

  const moveBot = useCallback((x, y) => {
    poseStream.send({x, y, angle: pose.angle})
  }, [poseStream, pose])

  return (
    <div className={ styles.container }>
      <MapDisplay pose={passedPose} moveBot={moveBot} showError={setError} />
      <RobotControl 
        pose={passedPose} 
        paused={paused} 
        togglePause={togglePause}
        error={error}
        />
    </div>
  )
}

export default RobotOverview;
