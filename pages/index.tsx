import Head from 'next/head';
import dynamic from 'next/dynamic';
import React from 'react';

import styles from '../styles/index.module.css';

const RobotOverview = dynamic(() => import('../components/RobotOverview'), { ssr: false });

const Home = () => {
  return (
    <div className={ styles.container }>
      <Head>
        <title>Robot Map tool</title>
        <meta name="description" content="A basic robot front-end" />
        <link rel="icon" href="/favicon.ico" />
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
      </Head>

      <header className={ styles.header }>
        Robot front-end
      </header>

      <main className={ styles.main }>
        <p className={ styles.placeholder }>
          Brought to you by editing{' '}
          <code className={ styles.code }>pages/index.tsx</code>{' '}
          with my implementation.
        </p>
        <RobotOverview />
      </main>

      <footer className={ styles.footer }>
        Powered by <a href="https://nextjs.org" target='_blank' rel="noreferrer">Next.js</a>
      </footer>
    </div>
  );
};

export default Home;
